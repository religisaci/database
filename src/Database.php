<?php
/**
 * Created by PhpStorm.
 * User: Libor
 * Date: 02.04.2019
 * Time: 17:50
 */

declare(strict_types = 1);

namespace Religisaci\Database;

use PDO;
use PDOStatement;
use PDOException;

class Database
{
	/** @var array */
	private static $connections = [];
	/** @var PDO */
	private $pdo;
	/** @var array */
	private $PDOTypes = [
		'boolean' => PDO::PARAM_BOOL,
		'NULL' => PDO::PARAM_NULL,
		'string' => PDO::PARAM_STR,
		'integer' => PDO::PARAM_INT,
	];
	/** @var */
	private $databaseName;
	/** @var string */
	private $host;
	/** @var string */
	private $userName;
	/** @var string */
	private $password;
	/** @var int */
	private $port;
	/** @var int */
	private $transactionDepth = 0;
	/** @var int */
	private $autocommit = NULL;

	private function __construct($databaseName, string $host, string $userName, string $password, int $port)
	{
		$this->databaseName = $databaseName;
		$this->host = $host;
		$this->userName = $userName;
		$this->password = $password;
		$this->port = $port;
	}

	/**
	 * @throws PDOException
	 */
	private function connect(): void
	{
		if($this->pdo === NULL)
		{
			$dsn = 'mysql:dbname=' . $this->databaseName . ';host=' . $this->host . ';port=' . $this->port . 'charset=utf8';
			try
			{
				$this->pdo = new PDO($dsn, $this->userName, $this->password);
				$this->pdo->exec("set names utf8");
			}
			catch(PDOException $e)
			{
				throw $e;
			}
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
	}

	/**
	 * @param string $databaseName
	 * @param string $host
	 * @param string $userName
	 * @param string $password
	 * @param int $port
	 * @param string $connectionName
	 */
	public static function crateConnection(string $databaseName, string $host, string $userName, string $password, int $port = 3306, string $connectionName = 'main')
	{
		if(isset(self::$connections[$connectionName]))
		{
			return self::$connections[$connectionName];
		}

		return self::$connections[$connectionName] = new self($databaseName, $host, $userName, $password, $port);
	}

	/**
	 * @param string $connectionName
	 * @return Database
	 * @throws \Exception
	 */
	public static function getConnection(string $connectionName = 'main'): self
	{
		if(isset(self::$connections[$connectionName]))
		{
			return self::$connections[$connectionName];
		}

		throw new \Exception('Connection to database not created. Please use Database::crateConnection method');
	}


	/**
	 * @param string $sql
	 * @return PDOStatement
	 * @throws \Exception
	 */
	public function query(string $sql): PDOStatement
	{
		$sqlParams = func_get_args();
		unset($sqlParams[0]);
		$sqlParams = array_values($sqlParams);
		return $this->queryParams($sql, $sqlParams);
	}

	/**
	 * @param string $sql
	 * @param array $sqlParams
	 * @return PDOStatement
	 * @throws \Exception
	 */
	public function queryParams(string $sql, array $sqlParams = []): PDOStatement
	{
		$this->connect();
		$sqlParameterIndex = -1;
		$counterOfQuestionMark = 0;
		$strings = [];
		$stringsCounter = 0;
		$sqlTemp = $sql;
		$sqlTemp = preg_replace_callback(
			'#("[^"]+")|(\'[^\']+\')#',
			function($matches) use (&$strings, &$stringsCounter)
			{
				$strings[$stringsCounter] = $matches[1] != '' ? $matches[1] : $matches[2];
				return 'sqlStringPlaceholder_' . ($stringsCounter++);
			},
			$sqlTemp
		);
		$sqlTemp = preg_replace_callback(
			'#([ ]*=[ ]*\?|[ ]*IN[ ]*\([ ]*\?[ ]*\))#',
			function($matches) use (&$sqlParameterIndex, &$counterOfQuestionMark, $sqlParams)
			{

				$sqlParameterIndex++;
				if(!isset($sqlParams[$sqlParameterIndex]))
				{
					$counterOfQuestionMark++;
					return $matches[1];
				}
				if(preg_match('#\([ ]*\?[ ]*\)#', $matches[1]) && is_array($sqlParams[$sqlParameterIndex]))
				{
					$counterOfQuestionMark += count($sqlParams[$sqlParameterIndex]);
					return ' IN (' . implode(', ', array_fill(0, count($sqlParams[$sqlParameterIndex]), '?')) . ')';
				}

				$counterOfQuestionMark++;
				return ' = ?';
			},
			$sqlTemp
		);
		$sqlTemp = preg_replace_callback(
			'#(sqlStringPlaceholder_([0-9]+))#',
			function($matches) use (&$strings)
			{
				return $strings[$matches[2]];
			},
			$sqlTemp
		);

		$comment = '';
		foreach(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS) as $step)
		{
			if(strpos($step['file'], __FILE__) !== FALSE)
			{
				continue;
			}
			$comment = '-- ' . $step['file'] . ':' . $step['line'] . "\n";
			break;
		}

		$sqlTemp = $comment . $sqlTemp;
		$statement = $this->pdo->prepare($sqlTemp);
		$counterOfParams = 0;
		foreach($sqlParams as $parameter)
		{
			if(is_array($parameter))
			{
				foreach($parameter as $arrayValue)
				{
					$counterOfParams++;
					$type = gettype($arrayValue);
					$type = isset($this->PDOTypes[$type]) ? $this->PDOTypes[$type] : PDO::PARAM_STR;
					$statement->bindValue($counterOfParams, $arrayValue, $type);
				}
				continue;
			}
			$counterOfParams++;
			$type = gettype($parameter);
			$type = isset($this->PDOTypes[$type]) ? $this->PDOTypes[$type] : PDO::PARAM_STR;
			$statement->bindValue($counterOfParams, $parameter, $type);
		}
		if($counterOfParams != $counterOfQuestionMark)
		{
			throw new \Exception('BAD Num of params.' . var_export($sql));
		}
		$statement->setFetchMode(PDO::FETCH_ASSOC);
		$statement->execute();

		return $statement;
	}

	/**
	 * @return int
	 */
	public function insertId(): int
	{
		return (int)$this->pdo->lastInsertId();
	}

	public function startTransaction()
	{
		if($this->transactionDepth++ > 0)
		{
			return;
		}
		$this->autocommit = $this->query('SHOW SESSION VARIABLES LIKE "autocommit"')->fetch()['Value'] === 'ON';
		$this->query('START TRANSACTION');
	}

	public function savePoint($identifier)
	{
		if(preg_match('/^[a-zA-Z][a-zA-Z0-9_]+$/', $identifier))
		{
			$this->query('SAVEPOINT `' . $identifier . '`');
		}
	}

	public function rollback()
	{
		if(--$this->transactionDepth > 0)
		{
			return;
		}
		$this->query('ROLLBACK');
		$this->query('SET AUTOCOMMIT = ' . ($this->autocommit === TRUE ? '1' : '0'));
	}

	public function commit()
	{
		if(--$this->transactionDepth > 0)
		{
			return;
		}

		$this->query('COMMIT');
		$this->query('SET AUTOCOMMIT = ' . ($this->autocommit === TRUE ? '1' : '0'));
	}


}